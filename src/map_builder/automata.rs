use super::MapArchitect;
use crate::prelude::*;

pub struct CellularAutomataArchitect {}

impl CellularAutomataArchitect {
    fn random_noise_map(&mut self, rng: &mut RandomNumberGenerator, map: &mut Map) {
        map.tiles.iter_mut().for_each(|t| {
            let roll = rng.range(0, 100);

            // To achieve chaos you want to bias the chaos toward making wall tiles
            if roll > 55 {
                *t = TileType::Floor;
            } else {
                *t = TileType::Wall;
            }
        })
    }

    fn count_neighbors(&self, x: i32, y: i32, map: &Map) -> usize {
        let mut neighbors = 0;
        // This checks all the tiles around the given x,y tile by ranging from -1 to 1 for x and y coordinates
        for iy in -1..=1 {
            for ix in -1..=1 {
                // skip 0, 0 as that would just be the current tile at x,y
                if !(ix == 0 && iy == 0) && map.tiles[map_index(x + ix, y + iy)] == TileType::Wall {
                    neighbors += 1;
                }
            }
        }

        neighbors
    }

    fn iteration(&mut self, map: &mut Map) {
        // create a copy since we will be making modifications to the map tiles
        let mut new_tiles = map.tiles.clone();
        let map_dimensions = map.dimensions();

        // avoid edges as the count neighbors will check outside the edge which will cause a crash
        for y in 1..map_dimensions.y - 1 {
            for x in 1..map_dimensions.x - 1 {
                let neighbors = self.count_neighbors(x, y, map);
                let idx = map.point2d_to_index(Point::new(x, y));
                if neighbors > 4 || neighbors == 0 {
                    new_tiles[idx] = TileType::Wall;
                } else {
                    new_tiles[idx] = TileType::Floor;
                }
            }
        }
        map.tiles = new_tiles;
    }

    fn find_start(&self, map: &Map) -> Point {
        let map_dimensions = map.dimensions();
        let center = Point::new(map_dimensions.x / 2, map_dimensions.y / 2);

        let closest_point = map
            .tiles
            .iter()
            .enumerate()
            .filter(|(_, t)| **t == TileType::Floor)
            // Calculate the Pythagoras distance from each tile to the center
            .map(|(idx, _)| {
                (
                    idx,
                    DistanceAlg::Pythagoras.distance2d(center, map.index_to_point2d(idx)),
                )
            })
            // Find the shortest distance from all the tiles
            .min_by(|(_, distance), (_, distance2)| distance.partial_cmp(&distance2).unwrap())
            // Return the index of the tile
            .map(|(idx, _)| idx)
            .unwrap();

        map.index_to_point2d(closest_point)
    }
}

impl MapArchitect for CellularAutomataArchitect {
    fn new(&mut self, rng: &mut RandomNumberGenerator) -> MapBuilder {
        let mut mb = MapBuilder {
            map: Map::new(),
            rooms: Vec::new(),
            monster_spawns: Vec::new(),
            player_start: Point::zero(),
            amulet_start: Point::zero(),
            theme: super::themes::DungeonTheme::new(),
        };

        self.random_noise_map(rng, &mut mb.map);

        // continually generate randomness
        for _ in 0..10 {
            self.iteration(&mut mb.map);
        }

        let start = self.find_start(&mb.map);

        mb.monster_spawns = mb.determine_monster_spawn_points(&start, rng, 50, 10.0);
        mb.player_start = start;
        mb.amulet_start = mb.find_most_distant();

        mb
    }
}
