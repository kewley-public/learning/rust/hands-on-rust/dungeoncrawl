use super::MapArchitect;
use crate::prelude::*;

pub struct EmptyArchitect {}

impl MapArchitect for EmptyArchitect {
    fn new(&mut self, rng: &mut bracket_lib::prelude::RandomNumberGenerator) -> MapBuilder {
        let mut mb = MapBuilder {
            map: Map::new(),
            rooms: Vec::new(),
            monster_spawns: Vec::new(),
            player_start: Point::zero(),
            amulet_start: Point::zero(),
            theme: super::themes::DungeonTheme::new(),
        };

        mb.fill(TileType::Floor);
        let map_dimensions = mb.map.dimensions();
        mb.player_start = Point::new(map_dimensions.x / 2, map_dimensions.y / 2);
        mb.amulet_start = mb.find_most_distant();
        for _ in 0..50 {
            mb.monster_spawns.push(Point::new(
                rng.range(1, map_dimensions.x),
                rng.range(1, map_dimensions.y),
            ));
        }

        mb
    }
}
