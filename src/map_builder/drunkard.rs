use super::MapArchitect;
use crate::prelude::*;

const STAGGER_DISTANCE: usize = 400;
const NUM_TILES: usize = (SCREEN_WIDTH * SCREEN_HEIGHT) as usize;
const DESIRED_FLOOR: usize = NUM_TILES / 3;

pub struct DrunkardsWalkArchitect {}

impl DrunkardsWalkArchitect {
    fn drunkard(&mut self, start: &Point, rng: &mut RandomNumberGenerator, map: &mut Map) {
        let mut drunkard_pos = start.clone();
        let mut distance_staggered = 0;

        // continues to run until you call a break
        loop {
            let drunk_idx = map.point2d_to_index(drunkard_pos);

            // miner "chips" away the wall
            map.tiles[drunk_idx] = TileType::Floor;

            // randomly choose the next tile to move to
            match rng.range(0, 4) {
                0 => drunkard_pos.x -= 1,
                1 => drunkard_pos.x += 1,
                2 => drunkard_pos.y -= 1,
                _ => drunkard_pos.y += 1,
            }

            if !map.in_bounds(drunkard_pos) {
                // exited the map
                break;
            }

            distance_staggered += 1;
            // exceeded the max distance
            if distance_staggered > STAGGER_DISTANCE {
                break;
            }
        }
    }

    fn mapping_completed(&self, map: &Map) -> bool {
        map.tiles.iter().filter(|t| **t == TileType::Floor).count() > DESIRED_FLOOR
    }
}

impl MapArchitect for DrunkardsWalkArchitect {
    fn new(&mut self, rng: &mut RandomNumberGenerator) -> MapBuilder {
        let mut mb = MapBuilder {
            map: Map::new(),
            rooms: Vec::new(),
            monster_spawns: Vec::new(),
            player_start: Point::zero(),
            amulet_start: Point::zero(),
            theme: super::themes::DungeonTheme::new(),
        };

        mb.fill(TileType::Wall);
        let map_dimensions = mb.map.dimensions();
        let center = Point::new(map_dimensions.x / 2, map_dimensions.y / 2);

        self.drunkard(&center, rng, &mut mb.map);

        // spawn each drunkard randomly until we feel the map is complete enough
        while !self.mapping_completed(&mb.map) {
            self.drunkard(
                &Point::new(
                    rng.range(0, map_dimensions.x),
                    rng.range(0, map_dimensions.y),
                ),
                rng,
                &mut mb.map,
            );

            let dijkstra_map = DijkstraMap::new(
                map_dimensions.x,
                map_dimensions.y,
                &vec![mb.map.point2d_to_index(center)],
                &mb.map,
                1024.0,
            );

            // ensure that we don't have any inaccessible tiles
            dijkstra_map
                .map
                .iter()
                .enumerate()
                // filter out inaccessible tiles, tiles have a distance of f32::MAX if they are inaccessible, so 2000 is a safe
                .filter(|(_, distance)| *distance > &2000.0)
                .for_each(|(idx, _)| mb.map.tiles[idx] = TileType::Wall);
        }

        mb.monster_spawns = mb.determine_monster_spawn_points(&center, rng, 50, 10.0);
        mb.player_start = center;
        mb.amulet_start = mb.find_most_distant();

        mb
    }
}
