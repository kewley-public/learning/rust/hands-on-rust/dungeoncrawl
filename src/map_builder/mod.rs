use crate::prelude::*;

mod automata;
mod drunkard;
mod empty;
mod prefab;
mod rooms;
mod themes;

use automata::CellularAutomataArchitect;
use drunkard::DrunkardsWalkArchitect;
use empty::EmptyArchitect;
use prefab::apply_prefab;
use rooms::RoomsArchitect;
use themes::{DungeonTheme, ForestTheme};

// Because we are working with Legion's resource system, the resources are accessible
// to systems that may be running on any thread, and may be running at any the same time.
// Legion requires that resources are Sync+Send safe
pub trait MapTheme: Sync + Send {
    fn tile_to_render(&self, tile_type: TileType) -> FontCharType;
}

trait MapArchitect {
    fn new(&mut self, rng: &mut RandomNumberGenerator) -> MapBuilder;
}

const NUM_ROOMS: usize = 20;
const MAX_ROOM_SIZE: usize = 10;
const UNREACHABLE: &f32 = &f32::MAX;

pub struct MapBuilder {
    pub map: Map,
    pub rooms: Vec<Rect>,
    pub monster_spawns: Vec<Point>,
    pub player_start: Point,
    pub amulet_start: Point,
    pub theme: Box<dyn MapTheme>,
}

impl MapBuilder {
    pub fn new(rng: &mut RandomNumberGenerator) -> Self {
        let mut place_amulet_in_fortress = false;
        let mut architect: Box<dyn MapArchitect> = match rng.range(0, 4) {
            0 => {
                println!("DrunkardsWalkArchitect chosen");
                Box::new(DrunkardsWalkArchitect {})
            }
            1 => {
                println!("RoomsArchitect chosen");
                Box::new(RoomsArchitect {})
            }
            2 => {
                println!("CellularAutomataArchitect chosen");
                Box::new(CellularAutomataArchitect {})
            }
            _ => {
                println!("EmptyArchitect chosen");
                place_amulet_in_fortress = true;
                Box::new(EmptyArchitect {})
            }
        };

        let mut mb = architect.new(rng);
        apply_prefab(&mut mb, rng, place_amulet_in_fortress);

        mb.theme = match rng.range(0, 2) {
            0 => {
                println!("Dungeon Theme Chosen");
                DungeonTheme::new()
            }
            _ => {
                println!("Forest Theme Chosen");
                ForestTheme::new()
            }
        };

        mb
    }

    // Fist make the map "un-moveable", we then chip away and build the rooms/cooridors
    pub fn fill(&mut self, tile: TileType) {
        // we have to de-reference with a * operator because we actually want to "change" what it's referencing too (i.e. point to a new value)
        self.map.tiles.iter_mut().for_each(|t| *t = tile);
    }

    fn find_most_distant(&self) -> Point {
        let starting_point = self.map.point2d_to_index(self.player_start);
        let screen_dimensions = self.map.dimensions();
        let dijkstra = DijkstraMap::new(
            screen_dimensions.x,
            screen_dimensions.y,
            &vec![starting_point],
            &self.map,
            1024.0,
        );

        self.map.index_to_point2d(
            dijkstra
                .map
                .iter()
                .enumerate() // we need the index at the end of this iteration for the index_to_point2d
                .filter(|(_, dist)| *dist < UNREACHABLE) // filter out any distances that are larger than the MAX float
                .max_by(|a, b| a.1.partial_cmp(b.1).unwrap()) // find the max distance
                .unwrap()
                .0,
        )
    }

    fn determine_monster_spawn_points(
        &self,
        start: &Point,
        rng: &mut RandomNumberGenerator,
        num_monsters: usize,
        min_distance_to_player: f32,
    ) -> Vec<Point> {
        let mut spawnable_tiles = self
            .map
            .tiles
            .iter()
            .enumerate()
            .filter(|(_, tile)| **tile == TileType::Floor)
            .filter(|(index, _)| {
                DistanceAlg::Pythagoras.distance2d(*start, self.map.index_to_point2d(*index))
                    > min_distance_to_player
            })
            .map(|(idx, _)| self.map.index_to_point2d(idx))
            .collect::<Vec<Point>>();

        let mut spawns = Vec::new();
        for _ in 0..num_monsters {
            let target_index = rng.random_slice_index(&spawnable_tiles).unwrap();
            spawns.push(spawnable_tiles[target_index].clone());
            spawnable_tiles.remove(target_index);
        }

        spawns
    }

    pub fn build_random_rooms(&mut self, rng: &mut RandomNumberGenerator) {
        let mut loop_count = 0;
        while self.rooms.len() < NUM_ROOMS {
            loop_count += 1;

            // generate a random room of size 10x10
            let room = Rect::with_size(
                rng.range(1, SCREEN_WIDTH - MAX_ROOM_SIZE as i32),
                rng.range(1, SCREEN_HEIGHT - MAX_ROOM_SIZE as i32),
                rng.range(2, MAX_ROOM_SIZE as i32),
                rng.range(2, MAX_ROOM_SIZE as i32),
            );

            // check for any overlap
            let mut overlap = false;
            for r in self.rooms.iter() {
                if r.intersect(&room) {
                    overlap = true;
                    break;
                }
            }

            if overlap {
                continue;
            }

            // Carve the room out from the block
            room.for_each(|p| {
                if let Some(idx) = self.map.try_idx(p) {
                    // player is allowed to walk on floors
                    self.map.tiles[idx] = TileType::Floor;
                }
            });
            self.rooms.push(room)
        }

        println!("Loop Count {loop_count}");
    }

    pub fn apply_vertical_tunnel(&mut self, y1: i32, y2: i32, x: i32) {
        use std::cmp::{max, min};

        // clever way to essentially loop on the distance from y1 to y2 or y2 to y1
        for y in min(y1, y2)..=max(y1, y2) {
            if let Some(idx) = self.map.try_idx(Point::new(x, y)) {
                // player is allowed to walk on floors
                self.map.tiles[idx as usize] = TileType::Floor
            }
        }
    }

    pub fn apply_horizontal_tunnel(&mut self, x1: i32, x2: i32, y: i32) {
        use std::cmp::{max, min};

        for x in min(x1, x2)..=max(x1, x2) {
            if let Some(idx) = self.map.try_idx(Point::new(x, y)) {
                self.map.tiles[idx as usize] = TileType::Floor
            }
        }
    }

    pub fn build_corridors(&mut self, rng: &mut RandomNumberGenerator) {
        let mut rooms = self.rooms.clone();

        // sort by centroid of the room, we want to connect adjacent rooms and not hop around the map
        rooms.sort_by(|a, b| a.center().x.cmp(&b.center().x));

        // using the ordered list connect the rooms, skip the first so we have two rooms to compare
        for (i, room) in rooms.iter().enumerate().skip(1) {
            let prev = rooms[i - 1].center();
            let new = room.center();

            // random probability to dig the cooridors
            if rng.range(0, 2) == 1 {
                self.apply_horizontal_tunnel(prev.x, new.x, prev.y);
                self.apply_vertical_tunnel(prev.y, new.y, new.x);
            } else {
                self.apply_vertical_tunnel(prev.y, new.y, prev.x);
                self.apply_horizontal_tunnel(prev.x, new.x, new.y);
            }
        }
    }
}

#[allow(dead_code)]
pub fn in_room(rooms: &Vec<Rect>, point: Point) -> bool {
    if let Some(_) = rooms.iter().find(|r| r.point_in_rect(point)) {
        true
    } else {
        false
    }
}
