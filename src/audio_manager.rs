use crate::prelude::*;

use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum AudioSound {
    Hit,
    Miss,
    MonsterDeath,
    Footstep,
    Background,
    ItemCollect,
    GameOver,
    GameWin,
    Birthday,
    ConsumePotion,
}

pub struct AudioManager {
    stream_handle: OutputStreamHandle,
    sounds: HashMap<AudioSound, PathBuf>,
    sinks: HashMap<AudioSound, Sink>,
}

impl AudioManager {
    pub fn new(stream_handle: OutputStreamHandle) -> Self {
        Self {
            stream_handle,
            sounds: HashMap::new(),
            sinks: HashMap::new(),
        }
    }

    pub fn add_sound(mut self, name: AudioSound, file_path: PathBuf) -> Self {
        self.sounds.insert(name, file_path);
        self
    }

    pub fn play_sound_once(&mut self, name: &AudioSound, volume: f32) {
        self.play_sound(name, volume, false);
    }

    pub fn play_sound_repeated(&mut self, name: &AudioSound, volume: f32) {
        self.play_sound(name, volume, true)
    }

    fn play_sound(&mut self, name: &AudioSound, volume: f32, repeat: bool) {
        if let Some(file_path) = self.sounds.get(name) {
            match File::open(file_path) {
                Ok(file) => {
                    let file = BufReader::new(file);
                    match Decoder::new(file) {
                        Ok(source) => {
                            let sink = Sink::try_new(&self.stream_handle).unwrap();
                            sink.set_volume(volume);

                            if repeat {
                                let repeated = source.repeat_infinite();
                                sink.append(repeated);
                            } else {
                                sink.append(source);
                            }

                            if repeat {
                                self.sinks.insert(*name, sink);
                            } else {
                                // Detach the sink if it's not repeating to let it play out
                                sink.detach();
                            }
                        }
                        Err(e) => {
                            eprintln!("Failed to decode audio: {}", e);
                        }
                    }
                }
                Err(e) => {
                    eprintln!("Failed to open audio file: {}", e);
                }
            }
        } else {
            eprintln!("Sound not found: {:?}", name);
        }
    }

    pub fn stop_sound(&mut self, name: &AudioSound) {
        if let Some(sink) = self.sinks.remove(name) {
            sink.stop();
        } else {
            eprintln!("No looping sound found with name: {:?}", name);
        }
    }
}

pub fn get_default_audio_device() -> Option<(OutputStream, OutputStreamHandle)> {
    match OutputStream::try_default() {
        Ok((stream, stream_handle)) => Some((stream, stream_handle)),
        Err(e) => {
            eprintln!("Failed to get default audio device: {}", e);
            None
        }
    }
}
