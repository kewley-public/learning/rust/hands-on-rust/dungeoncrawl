mod audio_manager;
mod camera;
mod components;
mod map;
mod map_builder;
mod spawner;
mod systems;
mod turn_state;

// Easier  way to organize modules that we expect to share throughout the codebase or to other authors
mod prelude {
    // External modules
    pub use bracket_lib::prelude::*;
    pub use legion::systems::CommandBuffer;
    pub use legion::world::SubWorld;
    pub use legion::*;
    pub use rodio::*;

    // Internal modules
    pub use crate::audio_manager::*;
    pub use crate::camera::*;
    pub use crate::components::*;
    pub use crate::map::*;
    pub use crate::map_builder::*;
    pub use crate::spawner::*;
    pub use crate::systems::*;
    pub use crate::turn_state::*;

    pub const SCREEN_WIDTH: i32 = 80;
    pub const SCREEN_HEIGHT: i32 = 50;
    pub const DISPLAY_WIDTH: i32 = SCREEN_WIDTH / 2;
    pub const DISPLAY_HEIGHT: i32 = SCREEN_HEIGHT / 2;
    pub const SOUND_EFFECT_VOLUME: f32 = 1.0;
    pub const BACKGROUND_VOLUME: f32 = 0.5;
    pub const MAX_LEVELS: u32 = 2;
}

use std::{collections::HashSet, path::PathBuf};

use prelude::*;

struct State {
    ecs: World,
    resources: Resources,
    stream_handle: OutputStreamHandle,
    input_systems: Schedule,
    player_systems: Schedule,
    monster_systems: Schedule,
}

fn initialize(ecs: &mut World, resources: &mut Resources, stream_handle: OutputStreamHandle) {
    let mut audio_manager = AudioManager::new(stream_handle)
        .add_sound(
            AudioSound::Background,
            PathBuf::from("resources/audio/background.mp3"),
        )
        .add_sound(
            AudioSound::Footstep,
            PathBuf::from("resources/audio/footstep.mp3"),
        )
        .add_sound(AudioSound::Hit, PathBuf::from("resources/audio/hit.mp3"))
        .add_sound(AudioSound::Miss, PathBuf::from("resources/audio/miss.mp3"))
        .add_sound(
            AudioSound::ItemCollect,
            PathBuf::from("resources/audio/item_pickup.mp3"),
        )
        .add_sound(
            AudioSound::Birthday,
            PathBuf::from("resources/audio/grunt_birthday_party.mp3"),
        )
        .add_sound(
            AudioSound::GameOver,
            PathBuf::from("resources/audio/game_over.mp3"),
        )
        .add_sound(
            AudioSound::GameWin,
            PathBuf::from("resources/audio/game_win.mp3"),
        )
        .add_sound(
            AudioSound::MonsterDeath,
            PathBuf::from("resources/audio/monster-death.mp3"),
        )
        .add_sound(
            AudioSound::ConsumePotion,
            PathBuf::from("resources/audio/consume_potion.mp3"),
        );

    audio_manager.play_sound_repeated(&AudioSound::Background, BACKGROUND_VOLUME);

    let mut rng = RandomNumberGenerator::new();
    let mut map_builder = MapBuilder::new(&mut rng);

    let mut map_level = 0;
    <(&mut Player, &mut Point)>::query()
        .iter_mut(ecs)
        .for_each(|(player, pos)| {
            player.map_level += 1;
            map_level = player.map_level;
            pos.x = map_builder.player_start.x;
            pos.y = map_builder.player_start.y;
        });

    if map_level == 0 {
        spawn_player(ecs, map_builder.player_start);
    }

    if map_level < MAX_LEVELS {
        let exit_idx = map_builder.map.point2d_to_index(map_builder.amulet_start);
        map_builder.map.tiles[exit_idx] = TileType::Exit;
    } else {
        spawn_amulet_of_yala(ecs, map_builder.amulet_start);
    }

    spawn_level(
        ecs,
        resources,
        &mut rng,
        map_level as usize,
        &map_builder.monster_spawns,
    );

    resources.insert(map_builder.map);
    resources.insert(Camera::new(map_builder.player_start));
    resources.insert(map_builder.rooms);
    resources.insert(rng);
    resources.insert(audio_manager);
    resources.insert(TurnState::AwaitingInput);
    resources.insert(map_builder.theme);
}

impl State {
    fn new(stream_handle: OutputStreamHandle) -> Self {
        let mut ecs = World::default();
        let mut resources = Resources::default();

        initialize(&mut ecs, &mut resources, stream_handle.clone());

        Self {
            ecs,
            resources,
            stream_handle,
            input_systems: build_input_scheduler(),
            player_systems: build_player_scheduler(),
            monster_systems: build_monster_scheduler(),
        }
    }

    fn handle_system_command(&self, ctx: &mut BTerm) {
        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::Q => ctx.quit(),
                _ => {}
            }
        }
    }

    fn game_over(&mut self, ctx: &mut BTerm, victory: bool) {
        ctx.set_active_console(2);
        if victory {
            ctx.print_color_centered(2, GREEN, BLACK, "You have won!");
            ctx.print_color_centered(
                4,
                WHITE,
                BLACK,
                "You put on the Amulet of Yala and feel its power course through your veins.",
            );
            ctx.print_color_centered(
                5,
                WHITE,
                BLACK,
                "Your town is saved, and you can return to your normal life.",
            );
        } else {
            ctx.print_color_centered(2, RED, BLACK, "Your quest has ended.");
            ctx.print_color_centered(
                4,
                WHITE,
                BLACK,
                "Slain by a monster, your hero's journey has come to a premature end.",
            );
            ctx.print_color_centered(
                5,
                WHITE,
                BLACK,
                "The Amulet of Yala remains unclaimed, and your home town is not saved.",
            );
            ctx.print_color_centered(
                8,
                YELLOW,
                BLACK,
                "Don't worry, you can always try again with a new hero.",
            );
        }

        ctx.print_color_centered(10, GREEN, BLACK, "Press 1 to play again.");
        ctx.print_color_centered(12, RED, BLACK, "Press Q to quit.");

        if let Some(VirtualKeyCode::Key1) = ctx.key {
            self.ecs = World::default();
            self.resources = Resources::default();

            initialize(
                &mut self.ecs,
                &mut self.resources,
                self.stream_handle.clone(),
            );
        }
    }

    fn advance_level(&mut self) {
        // 1 Remove all entities from ECS World that aren't either the player or items carried by the player
        // Keep the player
        let player_entity = *<Entity>::query()
            .filter(component::<Player>())
            .iter(&mut self.ecs)
            .next()
            .unwrap();

        let mut entities_to_keep = HashSet::new();
        entities_to_keep.insert(player_entity);

        // Keep all items the player is carrying
        <(Entity, &Carried)>::query()
            .iter(&self.ecs)
            .filter(|(_e, carry)| carry.0 == player_entity)
            .map(|(e, _carry)| *e)
            .for_each(|e| {
                entities_to_keep.insert(e);
            });

        // Remove other entities
        let mut cb = CommandBuffer::new(&mut self.ecs);
        for e in Entity::query().iter(&self.ecs) {
            if !entities_to_keep.contains(e) {
                cb.remove(*e);
            }
        }
        cb.flush(&mut self.ecs, &mut self.resources);

        // 2. Set the is_ditry flag on the player's FOV to ensure that the map renders correctly on the next turn.
        <&mut FieldOfView>::query()
            .iter_mut(&mut self.ecs)
            .for_each(|fov| fov.is_dirty = true);

        // 3. Generate a new level
        // 4. If current level number < 2 spawn an Exit, otherwise spawn the Amulet of YALA
        // 5. Finish setting up spawned monsters and resources as you did before
        initialize(
            &mut self.ecs,
            &mut self.resources,
            self.stream_handle.clone(),
        );
    }
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        self.handle_system_command(ctx);

        ctx.set_active_console(0);
        ctx.cls();
        ctx.set_active_console(1);
        ctx.cls();
        ctx.set_active_console(2);
        ctx.cls();

        // holds keyboard state
        self.resources.insert(ctx.key);

        // hold onto mouse state
        ctx.set_active_console(0); // ensures we receive coordinates for the correc terminal layer
        self.resources.insert(Point::from_tuple(ctx.mouse_pos()));

        let current_state = self.resources.get::<TurnState>().unwrap().clone();
        match current_state {
            TurnState::AwaitingInput => self
                .input_systems
                .execute(&mut self.ecs, &mut self.resources),
            TurnState::PlayerTurn => self
                .player_systems
                .execute(&mut self.ecs, &mut self.resources),
            TurnState::MonsterTurn => self
                .monster_systems
                .execute(&mut self.ecs, &mut self.resources),
            TurnState::GameOver => self.game_over(ctx, false),
            TurnState::Victory => self.game_over(ctx, true),
            TurnState::NextLevel => self.advance_level(),
        };

        render_draw_buffer(ctx).expect("Render error");
    }
}

fn main() -> BError {
    let mut num_array = vec![1, 2, 3, 4, 5];
    num_array.retain(|num| num % 2 == 0);

    println!("{:?}", num_array);

    let context = BTermBuilder::new()
        .with_title("Dungeon Crawler")
        .with_fps_cap(30.0)
        .with_dimensions(DISPLAY_WIDTH, DISPLAY_HEIGHT) // specifies the size of subsequent consoles you add
        .with_tile_dimensions(32, 32) // the size of each character in your font file (in our case is 32x32)
        .with_resource_path("resources/")
        .with_font("dungeonfont.png", 32, 32) // name of the font file and dimensions of each character
        .with_font("terminal8x8.png", 8, 8)
        .with_simple_console(DISPLAY_WIDTH, DISPLAY_HEIGHT, "dungeonfont.png")
        .with_simple_console_no_bg(DISPLAY_WIDTH, DISPLAY_HEIGHT, "dungeonfont.png")
        .with_simple_console_no_bg(SCREEN_WIDTH * 2, SCREEN_HEIGHT * 2, "terminal8x8.png")
        .build()?;

    if let Some((_, stream_handler)) = get_default_audio_device() {
        main_loop(context, State::new(stream_handler))
    } else {
        eprintln!("No audio device available");
        std::process::exit(1);
    }
}
