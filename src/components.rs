pub use crate::prelude::*;
use std::collections::HashSet;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Render {
    pub color: ColorPair,
    pub glpyh: FontCharType,
}

// Serves as a "tag" indiciating that an entity with this component is the player.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Player {
    pub map_level: u32,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Enemy;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct MovingRandomly;

// Too big to move into cooridors
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Size(pub usize);

impl Default for Size {
    fn default() -> Self {
        Self(0)
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct WantsToMove {
    pub entity: Entity,
    pub destination: Point,
    pub size: Option<Size>,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct WantsToAttack {
    pub attacker: Entity,
    pub victim: Entity,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct PlayAudio {
    pub sound: AudioSound,
    pub volume: f32,
    pub loop_audio: bool,
}

impl Default for PlayAudio {
    fn default() -> Self {
        Self {
            sound: AudioSound::Footstep,
            volume: 1.0,
            loop_audio: Default::default(),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct StopAudio {
    pub sound: AudioSound,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Health {
    pub current: i32,
    pub max: i32,
}

#[derive(Clone, PartialEq)]
pub struct Name(pub String);

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct ChasingPlayer;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Item;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct AmuletOfYala; // Yet another lost amulet

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct WantsToPickUpItem {
    pub item: Entity,
    pub who: Entity,
}

#[derive(Clone, Debug, PartialEq)]
pub struct FieldOfView {
    pub visible_tiles: HashSet<Point>,
    pub radius: i32,    // how many tiles each direction the entity can see
    pub is_dirty: bool, // optimization to not recalculate unless we have to
}

impl FieldOfView {
    pub fn new(radius: i32) -> Self {
        Self {
            visible_tiles: HashSet::new(),
            radius,
            is_dirty: true,
        }
    }

    // we need quick clones for recalculation of visible_tiles
    pub fn clone_dirty(&self) -> Self {
        Self {
            // used for building a new set of visible tiles
            visible_tiles: HashSet::new(),
            radius: self.radius,
            is_dirty: true,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct ProvidesHealing {
    pub amount: i32,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct ProvidesDungeonMap;

#[derive(Clone, PartialEq)]
pub struct Carried(pub Entity);

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct ActivateItem {
    pub used_by: Entity,
    pub item: Entity,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Damage(pub i32);

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Weapon;
