use crate::prelude::*;

#[system(for_each)]
pub fn play_audio(
    entity: &Entity,
    play_sound: &PlayAudio,
    #[resource] audio_manager: &mut AudioManager,
    commands: &mut CommandBuffer,
) {
    if play_sound.loop_audio {
        audio_manager.play_sound_repeated(&play_sound.sound, play_sound.volume);
    } else {
        audio_manager.play_sound_once(&play_sound.sound, play_sound.volume);
    }

    commands.remove(*entity);
}
