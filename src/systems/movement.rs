use crate::prelude::*;

// If we only have a single query the for_each will dervice said query using the paramemters specified
// and will run on each item returned... in this instance we query for entity/wantstomove and filter on Player.
#[system(for_each)]
#[read_component(Player)]
#[read_component(FieldOfView)]
pub fn movement(
    entity: &Entity,
    wants_to_move: &WantsToMove,
    #[resource] map: &mut Map,
    #[resource] camera: &mut Camera,
    // #[resource] rooms: &Vec<Rect>,
    ecs: &mut SubWorld,
    commands: &mut CommandBuffer,
) {
    // let entity_size = wants_to_move.size.unwrap_or_default().0;
    if map.can_enter_tile(wants_to_move.destination) {
        // if entity_size <= 2 || in_room(rooms, wants_to_move.destination) {
        commands.add_component(wants_to_move.entity, wants_to_move.destination);

        if let Ok(entry) = ecs.entry_ref(wants_to_move.entity) {
            if let Ok(fov) = entry.get_component::<FieldOfView>() {
                commands.add_component(wants_to_move.entity, fov.clone_dirty());

                // how you can access components outside of your query, must be specified in the read/_write_component
                if entry.get_component::<Player>().is_ok() {
                    camera.on_player_move(wants_to_move.destination);
                    fov.visible_tiles.iter().for_each(|pos| {
                        let index = map.point2d_to_index(*pos);
                        map.revealed_tiles[index] = true
                    });
                    commands.push((
                        (),
                        PlayAudio {
                            sound: AudioSound::Footstep,
                            volume: SOUND_EFFECT_VOLUME,
                            ..Default::default()
                        },
                    ));
                }
            }
        }

        commands.remove(*entity);
        // }
    }
}
