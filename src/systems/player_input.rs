use crate::prelude::*;

#[system] // auto-wraps a bunch of extra code so this integrates with legion
#[read_component(Point)]
#[read_component(Player)]
#[read_component(Enemy)]
#[read_component(Size)]
#[read_component(Enemy)]
#[read_component(Item)]
#[read_component(Carried)]
#[write_component(Health)]
pub fn player_input(
    ecs: &mut SubWorld,
    commands: &mut CommandBuffer,
    #[resource] key: &Option<VirtualKeyCode>,
    #[resource] turn_state: &mut TurnState,
) {
    if let Some(key) = key {
        let delta = match key {
            VirtualKeyCode::A | VirtualKeyCode::Left => Point::new(-1, 0),
            VirtualKeyCode::D | VirtualKeyCode::Right => Point::new(1, 0),
            VirtualKeyCode::W | VirtualKeyCode::Up => Point::new(0, -1),
            VirtualKeyCode::S | VirtualKeyCode::Down => Point::new(0, 1),
            VirtualKeyCode::Key1 => use_item(0, ecs, commands),
            VirtualKeyCode::Key2 => use_item(1, ecs, commands),
            VirtualKeyCode::Key3 => use_item(2, ecs, commands),
            VirtualKeyCode::Key4 => use_item(3, ecs, commands),
            VirtualKeyCode::Key5 => use_item(4, ecs, commands),
            VirtualKeyCode::Key6 => use_item(5, ecs, commands),
            VirtualKeyCode::Key7 => use_item(6, ecs, commands),
            VirtualKeyCode::Key8 => use_item(7, ecs, commands),
            VirtualKeyCode::Key9 => use_item(8, ecs, commands),
            _ => Point::zero(),
        };

        let mut players = <(Entity, &Point, &Size)>::query().filter(component::<Player>());
        let (player_entity, destination, player_size) = players
            .iter(ecs)
            // we know there is only one player so we don't need to perform any filtering
            .find_map(|(entity, pos, size)| Some((*entity, *pos + delta, *size)))
            .unwrap();

        if delta != Point::zero() {
            // return me entities with a Point component AND a Player Tag
            let mut enemies = <(Entity, &Point)>::query().filter(component::<Enemy>());
            let mut items = <(Entity, &Point)>::query().filter(component::<Item>());

            let mut hit_something = false;
            enemies
                .iter(ecs)
                .filter(|(_, pos)| **pos == destination)
                .for_each(|(entity, _)| {
                    hit_something = true;

                    commands.push((
                        (),
                        WantsToAttack {
                            attacker: player_entity,
                            victim: *entity,
                        },
                    ));
                });

            items
                .iter(ecs)
                .filter(|(_, pos)| **pos == destination)
                .for_each(|(entity, _)| {
                    hit_something = true;
                    commands.push((
                        (),
                        WantsToPickUpItem {
                            item: *entity,
                            who: player_entity,
                        },
                    ));
                });

            // if we didn't initiate attack move
            if !hit_something {
                commands.push((
                    (),
                    WantsToMove {
                        entity: player_entity,
                        destination,
                        size: Some(player_size),
                    },
                ));
            }

            *turn_state = TurnState::PlayerTurn;
        }
    }
}

fn use_item(n: usize, ecs: &mut SubWorld, commands: &mut CommandBuffer) -> Point {
    let player_entity = <Entity>::query()
        .filter(component::<Player>())
        .iter(ecs)
        .next()
        .unwrap();

    let item_entity = <(Entity, &Item, &Carried)>::query()
        .iter(ecs)
        .filter(|(_, _, carried)| carried.0 == *player_entity)
        .enumerate()
        .filter(|(item_count, (_, _, _))| *item_count == n)
        .find_map(|(_, (item_entity, _, _))| Some(*item_entity));

    if let Some(item_entity) = item_entity {
        commands.push((
            (),
            ActivateItem {
                used_by: *player_entity,
                item: item_entity,
            },
        ));
    }

    Point::zero()
}
