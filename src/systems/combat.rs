use crate::prelude::*;

#[system]
#[read_component(WantsToAttack)]
#[read_component(Player)]
#[read_component(Damage)]
#[read_component(Carried)]
#[write_component(Health)]
pub fn combat(
    ecs: &mut SubWorld,
    commands: &mut CommandBuffer,
    #[resource] rng: &mut RandomNumberGenerator,
) {
    let mut attackers = <(Entity, &WantsToAttack)>::query();

    let victims: Vec<(Entity, Entity, Entity)> = attackers
        .iter(ecs)
        .map(|(entity, attack)| (*entity, attack.attacker, attack.victim))
        .collect();

    // in rust it's best to not modify a collection while iterating
    victims.iter().for_each(|(message, attacker, victim)| {
        let is_player = ecs
            .entry_ref(*victim)
            .unwrap()
            .get_component::<Player>()
            .is_ok();

        let base_damage = ecs
            .entry_ref(*attacker)
            .unwrap()
            .get_component::<Damage>()
            .unwrap_or(&Damage(0))
            .0;

        let weapon_damage: i32 = <(&Carried, &Damage)>::query()
            .iter(ecs)
            .filter(|(carried, __)| carried.0 == *attacker)
            .map(|(_, dmg)| dmg.0)
            .sum();

        let final_damage = base_damage + weapon_damage;

        if let Ok(health) = ecs
            .entry_mut(*victim)
            .unwrap()
            .get_component_mut::<Health>()
        {
            let dice_roll = rng.roll_dice(10, 20);
            let hit_chance = dice_roll < 130;
            if hit_chance {
                commands.push((
                    (),
                    PlayAudio {
                        sound: AudioSound::Hit,
                        volume: SOUND_EFFECT_VOLUME,
                        ..Default::default()
                    },
                ));

                health.current -= final_damage;
                if health.current < 1 && !is_player {
                    commands.remove(*victim);
                    commands.push((
                        (),
                        PlayAudio {
                            sound: AudioSound::MonsterDeath,
                            volume: SOUND_EFFECT_VOLUME,
                            ..Default::default()
                        },
                    ));
                }
            } else {
                commands.push((
                    (),
                    PlayAudio {
                        sound: AudioSound::Miss,
                        volume: SOUND_EFFECT_VOLUME,
                        ..Default::default()
                    },
                ));
            }
        }

        commands.remove(*message);
    });
}
