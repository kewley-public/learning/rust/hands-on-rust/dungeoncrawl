use crate::prelude::*;

#[system(for_each)]
pub fn stop_audio(
    entity: &Entity,
    play_sound: &StopAudio,
    #[resource] audio_manager: &mut AudioManager,
    commands: &mut CommandBuffer,
) {
    audio_manager.stop_sound(&play_sound.sound);
    commands.remove(*entity);
}
