use std::cmp::min;

use crate::prelude::*;

#[system(for_each)]
#[read_component(ProvidesDungeonMap)]
#[read_component(ProvidesHealing)]
#[read_component(Carried)]
#[write_component(Health)]
pub fn use_item(
    entity: &Entity,
    activate_item: &ActivateItem,
    ecs: &mut SubWorld,
    commands: &mut CommandBuffer,
    #[resource] map: &mut Map,
) {
    if ecs
        .entry_ref(activate_item.item)
        .unwrap()
        .get_component::<ProvidesHealing>()
        .is_ok()
    {
        if let Ok(health) = ecs
            .entry_mut(activate_item.used_by)
            .unwrap()
            .get_component_mut::<Health>()
        {
            println!("Consume potion");
            health.current += 3;
            health.current = min(health.current, health.max);
            commands.push((
                (),
                PlayAudio {
                    sound: AudioSound::ConsumePotion,
                    volume: 3.0,
                    ..Default::default()
                },
            ));
        }
    } else if ecs
        .entry_ref(activate_item.item)
        .unwrap()
        .get_component::<ProvidesDungeonMap>()
        .is_ok()
    {
        println!("Activating dungeon map");
        map.revealed_tiles.fill_with(|| true);
    }

    commands.remove_component::<Carried>(activate_item.item);

    commands.remove(*entity);
}
