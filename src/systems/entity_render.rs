use crate::prelude::*;

#[system]
#[read_component(Point)]
#[read_component(Render)]
#[read_component(Player)]
#[read_component(FieldOfView)]
pub fn entity_render(ecs: &mut SubWorld, #[resource] camera: &Camera) {
    let mut draw_batch = DrawBatch::new();

    let mut field_of_view = <&FieldOfView>::query().filter(component::<Player>());
    let player_fov = field_of_view.iter(ecs).next().unwrap();

    draw_batch.target(1);

    // prepare the offset from the camera
    let offset = Point::new(camera.left_x, camera.top_y);

    // render entities
    <(&Point, &Render)>::query()
        .iter(ecs)
        .filter(|(pos, _)| player_fov.visible_tiles.contains(&pos))
        .for_each(|(pos, render)| {
            draw_batch.set(*pos - offset, render.color, render.glpyh);
        });

    // utilizing 5000 because the map may include 4000 elements. It's a good idea
    // to leave some room in case that changes or you add some user interface elements
    draw_batch.submit(5000).expect("Entity rendering failed");
}
