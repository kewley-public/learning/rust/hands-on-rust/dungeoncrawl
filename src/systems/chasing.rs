use crate::prelude::*;

#[system]
#[read_component(Point)]
#[read_component(ChasingPlayer)]
#[read_component(Health)]
#[read_component(Player)]
#[read_component(Size)]
#[read_component(FieldOfView)]
pub fn chasing(ecs: &SubWorld, commands: &mut CommandBuffer, #[resource] map: &Map) {
    let mut movers = <(Entity, &Point, &ChasingPlayer, &Size, &FieldOfView)>::query();
    let mut positions = <(Entity, &Point, &Health)>::query();
    let mut player = <(&Point, &Player)>::query();

    let player_pos = player.iter(ecs).next().unwrap().0;
    let player_idx = map.point2d_to_index(*player_pos);

    let search_targets = vec![player_idx]; // collection of target indexes on the map to search for
    let dijkstra_map = DijkstraMap::new(
        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        &search_targets,
        map,
        1024.0, // Max distance at which point it will stop calculating the map (preferrably choose a number large enough to cover the "relevant" map)
    );

    movers
        .iter(ecs)
        .filter(|(_, _, _, _, fov)| fov.visible_tiles.contains(&player_pos))
        .for_each(|(entity, pos, _, size, _)| {
            let idx = map.point2d_to_index(*pos);

            if let Some(destination) = DijkstraMap::find_lowest_exit(&dijkstra_map, idx, map) {
                let distance = DistanceAlg::Pythagoras.distance2d(*pos, *player_pos);
                // We choose 1.2 since it is larger than the 1.0 weight and 1.4 would imply that the monster can move diagonally which we don't want
                // Why 1.4? Pythagoras sqrt(1.0^2 + 1.0^2) ~ 1.4
                let destination = if distance > 1.2 {
                    map.index_to_point2d(destination)
                } else {
                    *player_pos
                };

                let mut attacked = false;
                positions
                    .iter(ecs)
                    .filter(|(_, target_pos, _)| **target_pos == destination)
                    .for_each(|(victim, _, _)| {
                        if ecs
                            .entry_ref(*victim)
                            .unwrap()
                            .get_component::<Player>()
                            .is_ok()
                        {
                            commands.push((
                                (),
                                WantsToAttack {
                                    attacker: *entity,
                                    victim: *victim,
                                },
                            ));
                        }
                        attacked = true;
                    });

                if !attacked {
                    commands.push((
                        (),
                        WantsToMove {
                            entity: *entity,
                            destination,
                            size: Some(*size),
                        },
                    ));
                }
            }
        })
}
