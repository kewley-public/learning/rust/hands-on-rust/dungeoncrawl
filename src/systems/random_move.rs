use crate::prelude::*;

#[system]
#[read_component(Point)]
#[read_component(MovingRandomly)]
#[read_component(Size)]
#[read_component(Health)]
#[read_component(Player)]
pub fn random_move(
    ecs: &mut SubWorld,
    commands: &mut CommandBuffer,
    #[resource] rng: &mut RandomNumberGenerator,
) {
    let mut movers = <(Entity, &Point, &Size)>::query().filter(component::<MovingRandomly>());
    let mut positions = <(Entity, &Point)>::query().filter(component::<Health>());

    movers.iter(ecs).for_each(|(entity, pos, size)| {
        let movement_delta = match rng.range(0, 5) {
            0 => Point::new(-1, 0),
            1 => Point::new(1, 0),
            2 => Point::new(0, -1),
            3 => Point::new(0, 1),
            _ => Point::zero(),
        };

        if movement_delta != Point::zero() {
            let destination = movement_delta + *pos;
            let mut attacked = false;
            positions
                .iter(ecs)
                .filter(|(_, target_pos)| **target_pos == destination)
                .for_each(|(victim, _)| {
                    if ecs
                        .entry_ref(*victim)
                        .unwrap()
                        .get_component::<Player>()
                        .is_ok()
                    {
                        commands.push((
                            (),
                            WantsToAttack {
                                attacker: *entity,
                                victim: *victim,
                            },
                        ));
                    }
                    attacked = true;
                });

            if !attacked {
                commands.push((
                    (),
                    WantsToMove {
                        entity: *entity,
                        destination,
                        size: Some(*size),
                    },
                ));
            }
        }
    });
}
