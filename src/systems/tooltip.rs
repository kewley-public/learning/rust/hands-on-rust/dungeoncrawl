use crate::prelude::*;

#[system]
#[read_component(Point)]
#[read_component(Name)]
#[read_component(Health)]
#[read_component(Player)]
#[read_component(FieldOfView)]
#[read_component(MovingRandomly)]
pub fn tooltip(ecs: &SubWorld, #[resource] mouse_position: &Point, #[resource] camera: &Camera) {
    let mut positions = <(Entity, &Point, &Name)>::query();

    let mut field_of_view = <&FieldOfView>::query().filter(component::<Player>());
    let player_fov = field_of_view.iter(ecs).next().unwrap();

    // Accomodate for offset of camera, mouse + the top left corner will give us the exact position
    let offset = Point::new(camera.left_x, camera.top_y);
    let map_position = offset + *mouse_position;

    // The tooltips layer is exactly four times larger than the monster layer
    let screen_pos = *mouse_position * 4;

    let mut draw_batch = DrawBatch::new();
    draw_batch.target(2);

    positions
        .iter(ecs)
        .filter(|(_, pos, _)| player_fov.visible_tiles.contains(pos))
        .filter(|(_, pos, _)| **pos == map_position)
        .for_each(|(entity, _, name)| {
            let display =
                if let Ok(health) = ecs.entry_ref(*entity).unwrap().get_component::<Health>() {
                    if ecs
                        .entry_ref(*entity)
                        .unwrap()
                        .get_component::<MovingRandomly>()
                        .is_ok()
                    {
                        format!("Drunk {} : {} hp", &name.0, health.current)
                    } else {
                        format!("{} : {} hp", &name.0, health.current)
                    }
                } else {
                    name.0.clone()
                };
            draw_batch.print(screen_pos, &display);
        });

    draw_batch.submit(9999).expect("Batch error");
}
