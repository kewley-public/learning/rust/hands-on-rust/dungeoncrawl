use crate::prelude::*;

#[system]
#[read_component(Player)]
#[read_component(FieldOfView)]
pub fn map_render(
    ecs: &SubWorld,
    #[resource] map: &Map,
    #[resource] camera: &Camera,
    #[resource] map_theme: &Box<dyn MapTheme>,
) {
    // similar to the BTerm context but this builds up a list of commands to batch
    let mut draw_batch = DrawBatch::new();
    // ensure that the map is rendered first
    draw_batch.target(0);

    let mut field_of_view = <&FieldOfView>::query().filter(component::<Player>());
    let player_fov = field_of_view.iter(ecs).next().unwrap();

    for y in camera.top_y..=camera.bottom_y {
        for x in camera.left_x..=camera.right_x {
            let pt = Point::new(x, y);
            if let Some(idx) = map.try_idx(pt) {
                if player_fov.visible_tiles.contains(&pt) || map.revealed_tiles[idx] {
                    let tint = if player_fov.visible_tiles.contains(&pt) {
                        WHITE
                    } else {
                        DARK_GRAY
                    };
                    let offset = Point::new(camera.left_x, camera.top_y);
                    let glyph = map_theme.tile_to_render(map.tiles[idx]);

                    draw_batch.set(pt - offset, ColorPair::new(tint, BLACK), glyph);
                }
            }
        }
    }
    draw_batch.submit(0).expect("Batch error");
}
