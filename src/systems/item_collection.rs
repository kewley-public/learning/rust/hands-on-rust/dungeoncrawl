use crate::prelude::*;

#[system(for_each)]
#[read_component(AmuletOfYala)]
#[read_component(ProvidesHealing)]
#[read_component(ProvidesDungeonMap)]
#[read_component(Weapon)]
#[read_component(Carried)]
#[read_component(Point)]
pub fn collect_item(
    entity: &Entity,
    item: &WantsToPickUpItem,
    commands: &mut CommandBuffer,
    ecs: &mut SubWorld,
    #[resource] turn_state: &mut TurnState,
) {
    let mut remove_item = false;
    if ecs
        .entry_ref(item.item)
        .unwrap()
        .get_component::<AmuletOfYala>()
        .is_ok()
    {
        remove_item = true;
        commands.push((
            (),
            PlayAudio {
                sound: AudioSound::GameWin,
                volume: BACKGROUND_VOLUME,
                ..Default::default()
            },
        ));
        *turn_state = TurnState::Victory;
    }

    if ecs
        .entry_ref(item.item)
        .unwrap()
        .get_component::<ProvidesHealing>()
        .is_ok()
    {
        commands.push((
            (),
            PlayAudio {
                sound: AudioSound::ItemCollect,
                volume: SOUND_EFFECT_VOLUME / 2.0,
                ..Default::default()
            },
        ));
        remove_item = true;
        commands.add_component(item.item, Carried(item.who));
    };

    if ecs
        .entry_ref(item.item)
        .unwrap()
        .get_component::<ProvidesDungeonMap>()
        .is_ok()
    {
        remove_item = true;
        commands.push((
            (),
            PlayAudio {
                sound: AudioSound::Birthday,
                volume: SOUND_EFFECT_VOLUME,
                ..Default::default()
            },
        ));
        commands.add_component(item.item, Carried(item.who));
    }

    if ecs
        .entry_ref(item.item)
        .unwrap()
        .get_component::<Weapon>()
        .is_ok()
    {
        let carried_weapons = <(&Carried, &Weapon)>::query()
            .iter(ecs)
            .filter(|(carried, _)| carried.0 == item.who)
            .count();
        if carried_weapons < 2 {
            commands.push((
                (),
                PlayAudio {
                    sound: AudioSound::Birthday,
                    volume: SOUND_EFFECT_VOLUME,
                    ..Default::default()
                },
            ));
            remove_item = true;
            commands.add_component(item.item, Carried(item.who));
        } else {
            commands.push((
                (),
                PlayAudio {
                    sound: AudioSound::Miss,
                    volume: SOUND_EFFECT_VOLUME,
                    ..Default::default()
                },
            ));
        }
    }

    if remove_item {
        commands.remove_component::<Point>(item.item);
    }

    commands.remove(*entity);
}
