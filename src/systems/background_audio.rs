use crate::prelude::*;

#[system]
pub fn background_audio(
    #[resource] turn_state: &mut TurnState,
    #[resource] audio_manager: &mut AudioManager,
) {
    match turn_state {
        TurnState::Victory | TurnState::GameOver => {
            audio_manager.stop_sound(&AudioSound::Background);
        }
        _ => {}
    }
}
