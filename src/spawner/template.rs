use crate::prelude::*;

use legion::systems::CommandBuffer;
use ron::de::from_reader;
use serde::Deserialize;
use std::collections::HashSet;
use std::fs::File;

#[derive(Clone, Deserialize, Debug)]
pub struct Template {
    pub entity_type: EntityType,
    pub levels: HashSet<usize>,
    pub frequency: i32, // how often we might see the entity
    pub name: String,
    pub glyph: char,
    pub provides: Option<Vec<(String, i32)>>, // any effects (e.g. healing)
    pub hp: Option<i32>,
    pub field_of_view: Option<usize>, // how far the entity can see
    pub size: Option<usize>,
    pub base_damage: Option<i32>,
}

#[derive(Clone, Deserialize, Debug, PartialEq)]
pub enum EntityType {
    Enemy,
    Item,
}

#[derive(Clone, Deserialize, Debug)]
pub struct Templates {
    pub entities: Vec<Template>,
}

impl Templates {
    pub fn load() -> Self {
        let file = File::open("resources/template.ron").expect("Failed opening file");
        let template: Templates = from_reader(file).expect("Unable to load templates");
        Self {
            entities: template.entities,
        }
    }

    pub fn spawn_entities(
        &self,
        ecs: &mut World,
        resources: &mut Resources,
        rng: &mut RandomNumberGenerator,
        level: usize,
        spawn_points: &[Point],
    ) {
        // stores a pointer to each entity, saving lots of memory
        let mut available_entities = Vec::new();
        self.entities
            .iter()
            .filter(|e| e.levels.contains(&level))
            .for_each(|t| {
                for _ in 0..t.frequency {
                    // increases frequency of entity being hit the more you add
                    // as we will use rng random_slice_entry below
                    available_entities.push(t)
                }
            });

        let mut commands = CommandBuffer::new(ecs);
        spawn_points.iter().for_each(|pt| {
            if let Some(entity) = rng.random_slice_entry(&available_entities) {
                self.spawn_entity(pt, entity, &mut commands);
            }
        });

        commands.flush(ecs, resources);
    }

    fn spawn_entity(&self, point: &Point, template: &Template, commands: &mut CommandBuffer) {
        // common traits all entities possess
        let entity = commands.push((
            point.clone(),
            Render {
                color: ColorPair::new(WHITE, BLACK),
                glpyh: to_cp437(template.glyph),
            },
            Name(template.name.clone()),
        ));

        match template.entity_type {
            EntityType::Item => commands.add_component(entity, Item {}),
            EntityType::Enemy => {
                commands.add_component(entity, Enemy {});
                commands.add_component(
                    entity,
                    FieldOfView::new(template.field_of_view.unwrap_or(4) as i32),
                );
                commands.add_component(entity, ChasingPlayer {});
                commands.add_component(
                    entity,
                    Health {
                        current: template.hp.unwrap(),
                        max: template.hp.unwrap(),
                    },
                );
                commands.add_component(entity, Size(template.size.unwrap_or(2)));
            }
        };

        if let Some(effects) = &template.provides {
            effects
                .iter()
                .for_each(|(provides, n)| match provides.as_str() {
                    "Healing" => commands.add_component(entity, ProvidesHealing { amount: *n }),
                    "MagicMap" => commands.add_component(entity, ProvidesDungeonMap {}),
                    _ => {
                        println!("Warning: we don't know how to provide {}", provides);
                    }
                })
        };

        if let Some(damage) = template.base_damage {
            commands.add_component(entity, Damage(damage));
            if template.entity_type == EntityType::Item {
                commands.add_component(entity, Weapon {});
            }
        };
    }
}
