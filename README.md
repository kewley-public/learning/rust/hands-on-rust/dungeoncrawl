# Dungeon Crawler

Following the HandsOnRust book to build a dungeon crawler. This readme
is primarily meant as a tldr; for my future self.

# Key Learning Points

Any useful info I find will be jotted down in the following sub documents.

* [bracket-lib](./docs/bracket_lib.md)
* [legion](./docs/legion.md)
* [rust](./docs/rust_specific.md)
* [game-theory](./docs/game_development.md)
