format:
    cargo fmt

run:
    cargo run

run-release:
    cargo run --release