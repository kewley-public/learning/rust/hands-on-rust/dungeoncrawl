# bracket-lib

Any useful tidbits I found while learning more about bracket-lib will be jotted down here.

## Rectangle

Useful object for building a rectangular shaped object with x,y,width,height.

### Instantiation

```rust
let x = 1;
let y = 2;
let width = 10;
let height = 5;
Rect::with_size(x, y, width, height);
```

### Useful operations

* `intersect(other_rectangle)` -> Will check if one rectangle intersects with another
* `for_each(closure)` -> Will iterate over each `point` of a `Rectangle`
* `center()` -> Will retrieve the center `Point` of the rectangle
  * We used this to build random rooms for our dungeon crawler
* `point_set` -> Will return all the points as a set

## Point

Structure wrapping an x,y position

### Instantiation

```rust
let point = Point::new(2, 3);
```

### Useful operations

* `arthimetic` -> You can add/subtract/multiply/etc... points together
    ```rust
    let offset = Point::new(2, 3);

    let position = Point::new(20, 23);

    let new_position = position - offset; // Point::new(18, 20)
    ```

## RandomNumberGenerator

Useful object that can generate various "randomness" within our application

### Instantiation

```rust
let rng = RandomNumberGenerator::new();
```

### Useful operations

#### `range(x, y)`

Generates a random number within, the range is `[x, y)` so x up to y - 1.

```rust
rng.range(0, 20); // will create a random number between 0 and 19 (inclusive)
```

#### `roll_dice(n, sides)`

Rolls `n` `sides`-sided dice and returns their result as a sum.

```rust
// Rolls two 10-sided dice and returns their result as a sum
rng.roll_dice(2, 10)
```

#### `random_slice_index(vector)`

Will randomly select an index from a given vector

```rust
let random_index = rng.random_slice_index(&vec![1, 2, 3, 4, 5]);
println!("{random_index}"); // could be anything from 0 to 4
```

## BTermBuilder

How we build the bracket terminal.

### Some builder methods

* `with_dimensions(x, y)` -> Specifies subsequent console dimensions that we add
* `with_tile_dimensions(32, 32)` -> Size of each character in your file (usually 32x32 but can vary)


## ColorPair

Helper class that stores both a foreground and a background color in a single struct.

### Instantiation

```rs
ColorPair::new(WHITE, BLACK)
```

## FontCharType

Store's a single character or glyph, so `to_cp437('@')` is a `FontCharType`

## DrawBatch

Batching service offered by bracket-lib for multi-threaded execution. Creates a buffer
of `deferred` rendering commands that can be executed in a specific order via the
`draw_batch.submit(sort_order)`.

### Usage

First you need to add `render_draw_buffer` to your tick command:

```rust
fn tick(&mut self, ctx: &mut BTerm) {
    // code...
    render_draw_buffer(ctx).expect("Render error");
    // more code...
}
```

Next you simply instantiate a `DrawBatch` and utilize it like the `ctx` in your render function like so:

```rust
#[system]
pub fn bob_render() {
    let mut draw_batch = DrawBatch::new();

    draw_batch.target(1); // bob is second on the list of rendering, we start at index 0 for `sort_order`
    // do rendering for bob
    // e.g. render
    draw_batch.set(Point::zero(), ColorPar::new(WHITE, BLACK), to_cp437('@'));
    // more code

    // final command in render
    draw_batch.submit(1).expect("Batch Error");
}
```

### `DrawBatch#bar_horizontal`

Useful help function that will place a bar on the screen (utilized a lot with health bars)

#### Usage

```rust
draw_batch.bar_horizontal(
    Point::zero(),
    SCREEN_WIDTH * 2,
    player_health.current,
    player_health.max,
    ColorPair::new(RED, BLACK),
);
```

## `mouse_pos`

A function on the `BTerm` that stores information on where your mouse is at a given
click as a `Tuple`.

E.g.
```rust
let mouse_pos = Point::from_tuple(ctx.mouse_pos());
```

## BaseMap

A `trait` that tells `bracket-lib` how one may travel across the map definiing useful functions for 
[Pathfinding](./game_development.md#pathfinding) and [Field Of View](./game_development.md#field-of-view). This
 is used in various algorithms that `bracket-lib` provides for you (e.g. [`Dijkstra's`](./game_development.md#dijkstra)).
It requires you to write two functions:

* `get_available_exits`
* `get_pathing_distance`

See the [map.rs](../src/map.rs) for full implementation details.

### `get_available_exits`

Looks at a tile and provides an itemized list of possible exist from that file.  You can even include
tiles at a much farther distance away (e.g. teleporter).

DO NOT, return the current tile as A* will choke if you do.

### `get_pathing_distance`

Estimates the distance between any two points. Normally Pythagorous is fine for gemoetric distance, but there
are a few others to choose from.

### `is_opaque`

Utilized for field-of-view algorithms see [Field Of View](./game_development.md#field-of-view) for theoritical knowledge.

`True` => You can see through the tile
`False` => You can not see through the tile

## Algorithm2D

A `trait` that translates between your map and generic map services offered by `baracket-lib`. This library
can not assume that you're storing your map in a row-first format (or even a vector), therefore
it requires you to implement a trait.

__NOTE__: Requires `BaseMap` trait to be implemented first

See the [map.rs](../src/map.rs) for full implementation details.

```rust
// Some default methods must be implemented as shown below to take advantage of all the other functionalities.
impl Algorithm2D for Map {
    fn dimensions(&self) -> Point {
        Point::new(SCREEN_WIDTH, SCREEN_HEIGHT)
    }

    fn in_bounds(&self, point: Point) -> bool {
        // defined on the Map impl, see the map.rs code...
        self.in_bounds(point)
    }
}
```

### Provided Functions

#### `dimensions`

Provides the size of the map

#### `in_bounds(x, y)`

Determines if an `x/y` coordinate is valid and contained within the map

#### `point2d_to_index(x, y)`

Is the same as our `map_index` function defined in [map.rs](../src/map.rs), it converts
a 2D `x/y` coordinate to a map index.

#### `index_to_point2d(index)`

Reciprocal of `point2d_to_index(x, y)`, when provided an `index` it will retun a tuple of `x/y` coordinates.


## DijkstraMap

Implementation of Dijkstra's pathfinding algorithm.

```rust
let search_targets = vec![player_idx]; // collection of target indexes on the map to search for
let dijkstra_map = DijkstraMap::new(
    SCREEN_WIDTH,
    SCREEN_HEIGHT,
    &search_targets,
    map,
    1024.0 // Max distance at which point it will stop calculating the map (preferrably choose a number large enough to cover the "relevant" map)
);
```

### `find_lowest_exit(dijkstraMap, idx, map)`

Given a the dijkstra map, the map index and the map it will find the exit with the lowest
cost. (it uses the list of exits built in the `BaseMap` `get_available_exits`)

```rust
if let Some(destination) = DijkstraMap::find_lowest_exit(&dijkstra_map, idx, map) {
    // Do code with destination (e.g. move to that destination)
}
```

## `field_of_view_set`

Calculates field-of-view for a map that supports Algorithm2D, returning a HashSet. This is a bit faster
than coercing the results into a vector, since internally it uses the set for de-duplication.

```rust
let mut views = <(&Point, &mut FieldOfView)>::query();

views
    .iter_mut(ecs)
    .filter(|(_, fov)| fov.is_dirty)
    .for_each(|(pos, fov)| {
        fov.visible_tiles = field_of_view_set(*pos, fov.radius, map);
        fov.is_dirty = false;
    });
```
