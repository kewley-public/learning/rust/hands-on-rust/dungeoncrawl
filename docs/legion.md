# legion

Crate that manages Entity Component Systems (ECS for short).

## Entity

Usually just an identification number that represents any game object (e.g. Player/Goblin/Dragon). There is no logic here.

## Components

A property that an Entity might have, for example a Player might have a `Position` property and a `Render` property.

## Systems

Query the entities and components and actually perform operations on them.

## Resources

Not mentioned in ECS but this is part of ECS where we have shared data available to multiple systems (e.g. map/camera).

## Why Is It Useful?

• Flexibility: You can easily build new monsters by mixing and matching properties. Want to add wings to your monster? Just attach the wing component.
• Reusability: The same properties (components) can be used for different monsters (entities). Melee can go on players and goblins.
• Manageability: It’s easier to change and manage small properties than a big monster. If you want to change how movement works, you only need to update the movement property.

## Usage

### Setup

Update your `State` to include the following:

```rust
struct State {
    ecs: World,
    resources: Resources,
    systems: Schedule,
}

impl State {
  pub fn new() -> Self {
    let mut ecs = World::default();
    let mut resources = Resources::default();

    // add resources...
    resources.insert(Map::new());
    resources.insert(Camera::new());
    // etc...

    Self {
      ecs,
      resources,
      // Usually hashed out in a seperate systems module
      systems: Schedule::builder()
        .build()
    }
  }
}
```

### Creating components

#### Component

Say we had a `components.rs` module. And we want a basic way to represent has a rendering an entity.

```rust
// Good to add Copy trait for ease of use
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Render {
  pub color: ColorPair,
  pub glpyh: FontCharType
}
```

#### Tag

We can think of these as a "component" but really it's a way to distinguish entities so we know exactly what
we are looking at with the entity.

Within the same `components.rs`:

```rust
// Creates a `tag` so that when we create an entity we will know it's of type `Goblin`
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Goblin;
```

### Creating new Entities with the `push` method

The `push` method on `ecs` (i.e. `World`) takes in a `component` or a tuple like structure that has the `tag` (aka `entity`) and all it's `properties` (e.g. `components`).

Assuming we have the `Goblin` entity from above

```rust
let entity: Entity = ecs.push(
  (
    Goblin,
    Point::zero(),
    Render {
      color: ColorPair::new(WHITE, BLACK),
      glyph: to_cp437('#')
    }
  )
);
```

### Systems example

How we might interact with the entities. This is multi-threaded as well.

#### Setup systems

Start with the basic builder:

```rust
Schedule::builder().build()
```

You will add various systems to the `Schedule` as you build them out.

#### Queries

Essentially a way to find out which components we are interested in within the `World` and this will pull out all `entities` that will match said components. It is required for entities to have **all** the components.

Think of this like a `join` in Database land as it will return all references to `components` grouped by `entity`.

Declared like so:

```rust
<(ComponentOfInterestOne, ComponentOfInterestTwo)>::query()
```

#### Filters

Additional requirements for an `entity` to show up. So for example say
we wanted all entities that had a `Point` and `Melee` `component` but **do not** want `entities` that have `Render`

```rust
<(Point, Melee)>::query().filter(component::<Render>())
```

#### Creating a System

Utilizes various `proc-macros`, proc-macros in Rust are ways to define boiler-plate code. Some examples we've already used are the
`derive`, when we add say `Clone` we get a `clone()` method added to struct.

##### `#[system]`

Transforms your function say `bob_function` to `bob_function_system` which adds all the necessary parts to integrate into legion's system.
We can then add it to the `Schedule::builder().build()` above like so:

```rust
Schedule::builder().add_system(bob_function_system()).build();
```

##### `#[write_component(Component)]`

Requests `writeable` access to a component type.

**NOTE**: Only a **single** system can write to a component at a given time. But many can read from components at the same time.

##### `#[read_component(Component)]`

Requests `read-only` access to a component type

##### `#[resource]`

Requests access to any of the `resources` that were added to the `World`.

##### SubWorld

Like the `World` but only contains access to components that you requested within the `proc-macros`.

##### Sample

```rust
#[system]
#[write_component(Player)]
#[write_component(Health)]
pub fn goblin_fight(
    ecs: &mut SubWorld,
    #[resource] key: &Option<VirtualKeyCode>,
    #[resource] map: &Map
) {}
```

## CommandBuffer

A special container to insert instructions for Legion to perform after the
system is finished. (e.g. add/move/remove entities). The commands are always
flushed at the end of a game tick as long as you are utilizing the `Scheduler`.

### Removing an entity with command buffer

```rust
#[system]
#[read_component(Player)]
#[read_component(Position)]
pub fn hooyah(ecs: &mut SubWorld, commands: &mut CommandBuffer) {
    let hooyah_position = Point::new(69, 69);

    let players = <(Entity, &Position)>::query().filter(component::<Player>());
    players.iter(ecs)
        .filter(|(_, pos) => *pos == hooyah_position)
        .for_each(|(entity, _) => commands.remove(*entity));
}
```

### Flushing with `flush`

If we want to ensure some commands happen before next system level operations happen
(e.g. maybe we remove the player before rending the player) we make a call to the
`flush()` operation.

```rust
Scheduler::builder()
    .add_system(hooyah::hooyah_system())
    .flush() // ensure hooyah commands are run
    .add_system(map::render_map_system())
    // more code...
    .build();
```

This also helps with multi-threading issues and ensures subsequent systems utilize up
to date information.

### Outside a system

You can call `CommandBuffer`'s outside of a system component, you just have to remember to call
the `flush` on the ECS and Resources.

Example:

```rust
let mut cb = CommandBuffer::new(&mut self.ecs);
for e in Entity::query().iter(&self.ecs) {
    if !entities_to_keep.contains(e) {
        cb.remove(*e);
    }
}
cb.flush(&mut self.ecs, &mut self.resources);
```

## `entry_ref` command

If you need to access components on an entity outside of a query you can utilize
the `entry_ref` on the `World/SubWorld` structs.

Example:

```rust
#[system]
#[read_component(Player)]
#[read_component(Point)]
pub fn something(ecs: &mut SubWorld) {
  let positions = <(Entity, &Point)>::query().iter(ecs).for_each(|(entity, point) {
    if ecs.entry_ref(entity).unwrap().get_component::<Player>().is_ok() {
      // do stuff
    }
    // more code
  });
}
```

## `entry_mut` command

Exactly the same as `entry_ref` but a mutable reference

```rust
victims.iter().for_each(|(message, victim)| {
    if let Ok(mut health) = ecs
        .entry_mut(*victim)
        .unwrap()
        .get_component_mut::<Health>()
    {
        health.current -= 1;
    }
})
```
