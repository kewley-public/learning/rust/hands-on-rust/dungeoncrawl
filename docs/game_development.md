# Game Development

Anything of interest that I want to jot down (e.g. theory).

## ECS (Entity Components Systems)

See more in-depth discussion [here](./legion.md#entity)

## Layers

There are many layers to display to the user, I'll list some out here that
I worked on within this game.

### Map Layer (Layer 0)

Typically the lowest indexed layer.

### Entity Layer (Layer 1)

Normally one level up from the map layer, this is where our entities our rendered.

### Heads-up Display (Layer 2)

Above the entity layer, I *think* it should be your highest layer. This is
where we will display names/power-ups/etc... to the user.

It's recommened you choose smaller font for HUDs.

## Messaging Systems

Typically it's better to dispatch messages from systems that can be handled
by say a root system or other systems. An example we did was `WantsToMove`
which dispatches from a player or a monster. Before messages we were handling
the movement logic in each individual system. This makes it harder as
time goes on and we want to add more features to movement (e.g. stun mechanics).

You can see our movement messaging system [here](../src/systems/movement.rs)

## Dirty Data

A lot of times you'll encounter a concept of `dirty`. This is an optimization technique that
is utilized throughout caching systems. When a calculation is expensive to do it's best to only
do said calculation when the object needs it. Hence, you'll see various `struct`s that might have
an `is_dirty` field, when that is `true` => it needs to be recalculated.

## Pathfinding

The general term in game development for an algorithm that helps an entity find its way from
point A to point B is *pathfinding*. Dijkstra is one such algorithm. `bracket-lib` has a few
pathfinding libraries available to choose from.

### Dijkstra

A Dijkstra map represents your map as a grid of numbers. You indicate "starting points" and
these are set to zero. The rest of the map is set to a larger number indicating that you can't
go there. Each starting point is scanned, and the tiles to which you can travel from that point are
given a value of 1. On the next iteration, open tiles that you haven't already visited get a value of 2.
This is repeated until every available tile on the map has a value, and that value tells you how far
away you are form the starting point.

```plaintext
function Dijkstra(graph, source):
    Q = priority queue (min-heap)
    dist = dictionary with default value infinity # keeps track of the shortest known distance from the source to each node.
    prev = dictionary with default value null # helps reconstructing the shortest path once the algorithm completes

    dist[source] = 0
    Q.insert(source, 0)

    while Q is not empty:
        u = Q.extract_min()  # Node with smallest distance
        
        for each neighbor v of u:
            alt = dist[u] + weight(u, v) # potential new shortest distance
            if alt < dist[v]:
                dist[v] = alt
                prev[v] = u
                Q.insert(v, alt)  # Insert or update v in Q with new distance

    return dist, prev
```

## Field of View

There are a few critical steps when defining the field of view for a given entity.

1. Denote the opacity of your tiles
2. Prepare component to store Visibility Data
3. A system that runs a field-of-view algorithm and stores the results in the new component type

### Tile Opactiy

First you must determine which tiles are see-through and which are opaque. The `bracket-lib` library
defines a method `is_opaque` on the `BaseMap` trait. For our use-case in our game we will be stating
that anything that is not a floor is opaque. See the [map.rs](../src/map.rs) for implementation details.

### Storing Visibility Data

Typically we store a `HashSet` for a collection of visibile tiles to the entity.

### Possible Struct

```rust
#[derive(Clone, Debug, PartialEq)]
pub struct FieldOfView {
    pub visible_tiles: HashSet<Point>,
    pub radius: i32,    // how many tiles each direction the entity can see
    pub is_dirty: bool, // optimization to not recalculate unless we have to
}
```

## Map Building Algorithms

Two of the most popular map generation algorithms are
`Cellular Automata` and `Drunkard's Walk`.

There are also `Prefabricated` which are just maps that are pre-determined that can be used
with the two algorithms.

### Cellular Automata Maps

Works by evolving pure chaos into a useful map.

Useful for producing organic-looking levels, such as a forest with clearings
or an old cavern network. Utilized for *Conway's Game of Life*.

Tries to simulate organic life. Each map tile independently lives (becomes a wall)
or dies (becomes open space) based on a count of its neighbors. You keep running iterations
until you have a usable map.

See [automata](../src/map_builder/automata.rs) for implementation details.

### Drunkard's Walk Maps

Produces very natural looking caverns worn away by erosion.

It works by randomly placing a drunken miner on a solid map. The miner digs randomly,
carving pathways into the map. Eventually, the miner either passes out (exceeding maximum turns)
or exist the map. You then check if the map is ready by counting the number of open tiles. If it
is not ready you spawn another drunken miner until the map is sufficiently open.

You need to decide how long your minder can stumble before passing out. Lower numbers tend to
give less open maps, larger numbers tend toward more open spaces.

You also need to decide how open you want your map to be (e.g. maybe 600 tiles open).

See [drunkard](../src/map_builder/drunkard.rs) for implementation details.

### Prefabricated Maps

When you want specific features of a map for part or even all of the map, you will implement
prefabricated (i.e. pre-built/pre-determined) maps.

See [prefab](../src/map_builder/prefab.rs) for implementation details
