# Rust specific

Anything useful I learned while working on this section specific to Rust will be documented in here.

## How to iterate on a distance

Say we are given some point x1 and another x2, we don't necessarily know which is larger, we can utilize the `std::cmp{min, max}` functions defined in the standard comparison library

```rust
for x in min(x1, x2)..=max(x1, x2) {
// do stuff
}
```

## Iterator `sort_by`

Will sort the vector (and modify the vector) by some defined comparison closure

### Example (sorting rectangles by center)

```rust
let rooms = vec![];
// populate rooms
// now we sort, this WILL modify the ordering of the rooms vector
rooms.sort_by(|a, b| a.center().x.cmp(&b.center().x));
```

## Iterator `enumerate`

This will iterate and pass back an index as a tuple.

```rust
for (index, value) in rooms.enumerate() {
    // do stuff
}
```

## Iterator `collect`

This will take the final result and convert it to a vector.

```rust
let mut attackers = <(Entity, &WantsToAttack)>::query();
// converts the attackers to a vector of attacker/victim pairs
let victims: Vec<(Entity, Entity)> = attackers
    .iter(ecs)
    .map(|(entity, attack)| (*entity, attack.victim))
    .collect(); // converts the iterator to a vector
```

## Iterator `skip(number)`

This will skip up to the number within the iteration, only useful if we want to skip the first to do comparisons imo.

```rust
let array = vec![1, 2, 3];
array.iter().skip(1).for_each(|x| println!("Value {x}")); // will print 2, 3
```

## Iterator `nth(index)`

Retrieves the index of the iterator

```rust
let array = vec![1, 2, 3];
let nth_value = array.iter().nth(1);
println("{nth_value}"); // will print 2
```

## Iterator `next()`

Retrieves the next item in the iterator, sometimes more useful to do `.next()` vs `nth(0)` for the first element.

```rust
let array = vec![1, 2, 3];
let next_value = array.iter().next();
println("{next_value}"); // will print 1
```

## Iterator `find_map(fitler_fn)`

Merges the `filter` and the `map` together, returning a single Optional result.

Usage:

```rust
let numbers = vec![1, 2, 3, 4, 5];

// Find the first even number and map it to its square
let result = numbers.iter().find_map(|&x| {
    if x % 2 == 0 {
        Some(x * x)
    } else {
        None
    }
});

// In this case the result will be Some(4) since 2 was the first even number
match result {
    Some(square) => println!("Found and squared: {}", square), // prints 4
    None => println!("No even number found"),
}
```

## Iterator `max_by(compare_fn)`

Different than `max` as this is utilized when we are iterating over structures that
 need a more complex approach.

```rust
#[derive(Debug, Copy, Clone)]
struct KewlStruct {
    score: i32,
}

let scores = vec![
    KewlStruct { score: 12 },
    KewlStruct { score: 32 },
    KewlStruct { score: 89 },
    KewlStruct { score: 69 },
];

let max_score = scores
    .iter()
    // &&KewlStruct, &&KewlStruct
    .max_by(|a, b| a.score.partial_cmp(&b.score).unwrap())
    .unwrap();
println!("{:?}", max_score); // KewlStruct { score: 89 }
```

## Iterator `retain(retain_fn)`

Modifies the vector and retains only items that meet the retain_fn condition.

```rust
let mut num_array = vec![1, 2, 3, 4, 5];

// will modify the array, only keeping evens
num_array.retain(|num| num % 2 == 0);

println!("{:?}", num_array); // [2, 4]
```

## Filling an array with default values

Say we want to fill an array with a bunch of 0's of size 32:

```rust
let some_array = vec![0 ; 32];
```

## Preludes

Useful way of organizing all your crate information into a single prelude module that can be included all over.
See the example within the [main.rs](../src/main.rs). Now throughout our code base we just call `use crate::prelude::*`.

## Struct(s)

### Tuple structs

You can define a struct like so:

```rust
pub struct Person(pub String, pub i32, pub String);
```

When you do this you don't need to give any field names like you would in other structs.

To access members of this struct you treat it just like a Tuple

```rust
let person = Person("Mark".to_string(), 34, "Minneosta".to_string());
person.0 // Mark
person.1 // 34
person.2 // Minnesota
```

## Traits

A *trait* tells the Rust compiler about functionality a particular type has and
can share with other types. Think of them like interfaces/abstract classes in other
programming languages.

In practical terms, this means that a trait offers functionality to anything that
meets its requirements without having to know about the specifics of your program.

For example, the `GameState` trait we implemented in [main.rs](../src/main.rs) doesn't
know anything about your game other than it promises that a `tick` function will be available.

```rust
// code
impl GameState for State {
    // method required to tell the compiler State behaves like GameState because it implements `tick`.
    fn tick(&mut self, ctx: &mut BTerm) {
        // code
    }
}
// code
```

## Loops

Loops will run indefinitely until you call `break`

```rust
let max_loop = 200;
let mut current_loop = 0;

// Loops 200 times
loop {
    current_loop += 1;
    if current_loop > max_loop {
        break;
    }
}
```

Example in our code can be found in [drunkard](../src/map_builder/drunkard.rs)

## Boxing

When you are dealing with an object that might be anything that implements a `trait` you have
to put it within a `Box`. A `Box` is a *smart pointer*, it creates the `trait` type you requested and
holds a pointer to it. When a `Box` is destroyed, it takes care of deleting the contained object.

You can put (almost) anything into a `Box`. While the `Box`'s content may vary, the type needs to be
annotated with `dyn`. `dyn` is short for *dynamic dispatch*. When you use this Rust will allow you to
put any type that implements the named trait into the `Box`. Therefore when you use this variable you will
be able to access the functionality for the `trait`.

E.g.

```rust
trait SuperHuman {
    pub fn can_fly(&self) -> bool;
}

struct ElastiGirl {}
struct MrIncredible {}
struct OmniMan {}

impl SuperHuman for ElastiGirl {
    pub fn can_fly(&self) {
        false
    }
}

impl SuperHuman for MrIncredible {
    pub fn can_fly(&self) {
        false
    }
}

impl SuperHuman for OmniMan {
    pub fn can_fly(&self) {
        true
    }
}

fn main() {
    let rng = RandomNumberGenerator::new();

    // Here we specify the Box and the dynamic dispatch
    let super_human: Box<dyn SuperHuman> = match rng.range(0, 3) {
        0 => Box::new(ElastiGirl {}),
        1 => Box::new(MrIncredible {}),
        _ => Box::new(OmniMan {}),
    };

    // Rust knows because of tye dynamic dispatch that this box has the `can_fly` function
    if super_human.can_fly() {
        println!("Yea I'm OP");
    } else {
        println!("Can't fly");
    }
}
```

## Sync + Send

Rust emphasizes "fearless concurrency", which means you can write multi-threaded code
without fearing that it will do something terrible. A large part of how Rust enforces
safe concurrency is with the `Sync` and `Send` traits.

Most of the times you don't have to implement `Sync`/`Send` as Rust will automatically
do this for you. Normally you add this to `traits` to `constrain` the types that implement
it to be threadsafe.

### Sync

If an object implements `Sync`, you can safely access it from multiple threads.

### Send

If an object implements `Send`, you can safely share it between threads.

## Rusty Object Notation (RON)

Like JSON but for Rust. An example can be found in [template.ron](../resources/template.ron)

Example:

```rust
Templates(
    entities : [
        Template(
            entity_type: Item,
            name: "Healing Potion", 
            glyph: "!",
            levels: [0, 1, 2],
            provides: Some([("Healing", 6)]),
            frequency: 2
        ),
    ],
)
```

## Serde

Rust's crate library to assist with the process of *serializing* and *deserializing* data.

## Releasing/Packaging

Utilizing `Release` mode will run your program *much* *much* faster. You can run it using the
`--release` flag like so:

```bash
cargo run --release
```

### Link Time Optimization (LTO)

A futher optimization technique, normal compilation only optimizes the contents
of each module. LTO analyzes the entire program, including all of your crate dependencies
(and their dependencies).

To enable simple add the following to your `Cargo.toml`

```toml
[profile.release]
lto = "thin"
```

### Packaging Up For Release

Run the following steps:

```bash
cargo clean && cargo build --release
```

This will create a `target` folder, in there you will find the `dungeoncrawl` executable.

Create a new directory (e.g. `DungeonCrawl`) copy that executable and the `resources` directory there

Then run the following:

```bash
chmod +x DungeonCrawl/dungeoncrawl
./DungeonCrawl/dungeoncrawl
```

This should start up your game. Now you can compress it and distribute it as you desire.
